# TLauncher
This is a repo that i made to install TLauncher on Arch Linux directly from their [site](https://tlauncher.org/).

## How to install

1. Change the permissions of the script
    ```c 
    chmod +x ./install.sh
    ```
2. Install tlauncher
    ```c
    ./install.sh
    ```

## Usage
After the installation, you can just type `tlauncher` in the terminal to launch tlauncher.

If you wanna update type `tlauncher --update` or `tlauncher -u`, for downloading the last version of tlauncher _(It's not the best way of doing this, but it will do it for now)_. 